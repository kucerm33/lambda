package App

import App.Lambda.Expression


class Evaluator
{

	fun evaluate(e: Expression): Expression
	{
		var res = e
		var tmp: Expression? = e
		while (tmp != null) {
			res = tmp
			tmp = res.evaluate()
		}
		return res
	}

}
