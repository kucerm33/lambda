package App.Lambda


class Application (private val e1: Expression, private val e2: Expression) : Expression
{

	override fun evaluate(): Expression?
	{
		if (e1 is Function) {
			return e1.evaluate(e2) // normal
		}
		val r1 = e1.evaluate()
		val r2 = e2.evaluate()
		if (r1 == null && r2 == null) {
			return null
			// return if (e1 is Function) e1.evaluate(e2) else null // applicative
		}
		return Application(r1 ?: e1, r2 ?: e2)
	}


	override fun propagate(variable: VariableId, value: Expression): Application
	{
		return Application(e1.propagate(variable, value), e2.propagate(variable, value))
	}


	override fun bind(variable: VariableId): Application
	{
		return Application(e1.bind(variable), e2.bind(variable))
	}


	override fun rebind(from: VariableId, to: VariableId): Application
	{
		return Application(e1.rebind(from, to), e2.rebind(from, to))
	}


	override fun duplicate(): Application
	{
		return Application(e1.duplicate(), e2.duplicate())
	}


	override fun evaluateNumbers(): Application
	{
		return Application(e1.evaluateNumbers(), e2.evaluateNumbers())
	}


	fun asNumber(one: VariableId, zero: VariableId): Int?
	{
		if (e1 !is Variable || !e1.bindsTo(one)) {
			return null
		}
		if (e2 is Variable && e2.bindsTo(zero)) {
			return 1
		}
		if (e2 is Application) {
			return e2.asNumber(one, zero)?.plus(1)
		}
		return null
	}


	override fun toString(): String
	{
		return "($e1 $e2)"
	}

}
