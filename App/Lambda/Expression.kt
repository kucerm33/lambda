package App.Lambda


interface Expression
{

	fun evaluate(): Expression?

	fun propagate(variable: VariableId, value: Expression): Expression

	fun bind(variable: VariableId): Expression

	fun rebind(from: VariableId, to: VariableId): Expression

	fun duplicate(): Expression

	fun evaluateNumbers(): Expression

	override fun toString(): String

}
