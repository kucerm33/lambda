package App.Lambda


class Function(val variable: VariableId, val body: Expression, private val helper: FunctionHelper) : Expression
{

	override fun evaluate(): Expression?
	{
		val res = body.evaluate()
		return if (res != null) helper.edit(this, res) else null
	}


	fun evaluate(param: Expression): Expression
	{
		return body.propagate(variable, param)
	}


	override fun propagate(variable: VariableId, value: Expression): Function
	{
		return helper.edit(this, body.propagate(variable, value))
	}


	override fun bind(variable: VariableId): Function
	{
		return helper.edit(this, body.bind(variable))
	}


	override fun rebind(from: VariableId, to: VariableId): Function
	{
		return helper.edit(this, body.rebind(from, to))
	}


	override fun duplicate(): Function
	{
		return helper.duplicate(this)
	}


	override fun evaluateNumbers(): Expression
	{
		if (body is Function) {
			val n = body.asNumber(variable)
			if (n != null) {
				return Variable(n.toString())
			}
		}
		return helper.edit(this, body.evaluateNumbers())
	}


	private fun asNumber(parentVar: VariableId): Int?
	{
		if (body is Variable && body.bindsTo(variable)) {
			return 0
		}
		if (body is Application) {
			return body.asNumber(parentVar, variable)
		}
		return null
	}


	override fun toString(): String
	{
		return "(λ${variable.name}. $body)"
	}

}
