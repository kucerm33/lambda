package App.Lambda


class FunctionHelper
{

	private var varSequence = 0


	fun create(varName: String, body: Expression): Function
	{
		val varId = VariableId(varName, varSequence++)
		return Function(varId, body, this).bind(varId)
	}


	fun edit(f: Function, body: Expression = f.body): Function
	{
		return Function(f.variable, body, this)
	}


	fun duplicate(f: Function): Function
	{
		val varId = VariableId(f.variable.name, varSequence++)
		return Function(varId, f.body.duplicate(), this).rebind(f.variable, varId)
	}

}
