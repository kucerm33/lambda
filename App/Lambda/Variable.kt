package App.Lambda


class Variable (private val name: String, private val id: Int? = null) : Expression
{

	override fun evaluate(): Expression?
	{
		return null
	}


	override fun propagate(variable: VariableId, value: Expression): Expression
	{
		if (id == variable.id) {
			return value.duplicate()
		}
		return this
	}


	override fun bind(variable: VariableId): Variable
	{
		if (id == null && this.name == variable.name) {
			return Variable(name, variable.id)
		}
		return this
	}


	override fun rebind(from: VariableId, to: VariableId): Variable
	{
		if (id == from.id) {
			return Variable(name, to.id)
		}
		return this
	}


	fun bindsTo(variable: VariableId): Boolean
	{
		return id == variable.id
	}


	override fun duplicate(): Variable
	{
		return this
	}


	override fun evaluateNumbers(): Variable
	{
		return this
	}


	override fun toString(): String
	{
		return name
	}

}
