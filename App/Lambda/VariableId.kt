package App.Lambda


data class VariableId (val name: String, val id: Int)
