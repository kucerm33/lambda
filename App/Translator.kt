package App

import App.Lambda.*
import java.util.*


class Translator (private val functionHelper: FunctionHelper)
{

	private val DELIMITERS = "(" + arrayOf("\\(", "\\)", "\\.", "λ").joinToString("|") + ")"

	private val macros = hashMapOf<String, Expression>()


	init {
		addMacro("++", "(λx. λs. λz. s (x s z))")
		addMacro("--", "(λx. λs. λz. x (λf.λg.g (f s)) (λg.z) (λm.m))")
		addMacro("+", " (λx. λy. x ++ y)")
		addMacro("-", " (λx. λy. y -- x)")
		addMacro("*", "(λx. λy. λz. x (y z))")
		addMacro("TRUE", "(λt. λf. t)")
		addMacro("FALSE", "(λt. λf. f)")
		addMacro("NOT", "(λx . x FALSE TRUE)")
		addMacro("AND", "(λx. λy. x y FALSE)")
		addMacro("OR", "(λx. λy. x TRUE y)")
		addMacro("ZERO", "(λx. x FALSE NOT FALSE)")
		addMacro("Y", "(λf. (λx. f (x x))(λx. f (x x)))")
	}


	fun addMacro(name: String, body: String)
	{
		macros[name] = translate(body)
	}


	fun translate(code: String): Expression
	{
		val tokens = LinkedList(code.split("(\\s|(?<=$DELIMITERS)|(?=$DELIMITERS))".toRegex()).filter { it != "" })
		val res: Expression
		try {
			res = doTranslate(tokens)
		} catch (e: NoSuchElementException) {
			throw TranslatorException("Unexpected end of expression", e)
		} catch (e: NullPointerException) {
			throw TranslatorException("Unexpected end of expression", e)
		} finally {
			if (tokens.isNotEmpty()) {
				throw TranslatorException("Unexpected token `${tokens.peek()}`")
			}
		}
		return res
	}


	private fun doTranslate(tokens: Queue<String>): Expression
	{
		var result: Expression? = null
		while (tokens.isNotEmpty()) {
			if (tokens.peek() == ")") break
			val token = tokens.remove()
			val e = when (token) {
				in macros -> macros[token]!!
				"(" -> {
					val sub = doTranslate(tokens)
					assertToken(")", tokens.remove())
					sub
				}
				"λ", "lambda" -> {
					val variable = tokens.remove()
					assertToken(".", tokens.remove())
					val body = doTranslate(tokens)
					makeLambda(variable, body)
				}
				else -> makeInt(token) ?: Variable(token)
			}
			result = if (result == null) e else Application(result, e)
		}
		return result!!
	}


	private fun makeLambda(variable: String, body: Expression) = functionHelper.create(variable, body)


	private fun makeInt(token: String): Expression?
	{
		val n = token.toIntOrNull() ?: return null
		var body: Expression = Variable("z")
		for (i in 1..n) {
			body = Application(Variable("s"), body)
		}
		return makeLambda("s", makeLambda("z", body))
	}


	private fun assertToken(expected: String, actual: String)
	{
		if (actual != expected) {
			throw TranslatorException("Unexpected token `$actual`, expecting `$expected`")
		}
	}

}
