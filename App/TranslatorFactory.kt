package App

import App.Lambda.FunctionHelper


class TranslatorFactory
{

	fun create() = Translator(FunctionHelper())

}
