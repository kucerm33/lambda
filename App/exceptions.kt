package App


class TranslatorException (override val message: String?, override val cause: Throwable? = null) : Exception ()
