package App

import java.io.BufferedReader
import java.io.InputStreamReader


fun main(args: Array<String>)
{
	val translator = TranslatorFactory().create()
	val evaluator = Evaluator()
	BufferedReader(InputStreamReader(System.`in`)).forEachLine {
		if (it.startsWith("#define")) {
			val (_, name, body) = it.split("\\s".toRegex(), 3)
			translator.addMacro(name, body)
		} else {
			val res = evaluator.evaluate(translator.translate(it)).evaluateNumbers()
			println(res)
		}
	}
}
