import App.Evaluator
import App.TranslatorFactory


data class Assert (val source: String, val expected: String)


val TESTS = arrayOf(
	Assert("(lambda f . (lambda x . f x))(lambda y . y)", "(λx. x)"),
	Assert("((λx.λy.x)y)z", "y"),
	Assert("(λs. λq. s q q) (λq.q) q", "(q q)"),
	Assert(" (λx. x i)((λz. (λq. q) z) h)", "(h i)"),
	Assert("(λx. x i)((λz. (λq. q z)) h)", "(i h)"),
	Assert("(λx. x o j)((λy. (λz. z h)y)a)", "(((a h) o) j)"),
	Assert("(λx. (λx. (λx. x x x)(b x)x)(a x))c", "((((b (a c)) (b (a c))) (b (a c))) (a c))"),
	Assert("(λw.(λx.(λy.w y a) (u w)) b) y", "((y (u y)) a)"),
	Assert("(λx. (λw.(λy. w y w)b))((λx. x x x)(λx. x x x)) ((λz.z)a)", "((a b) a)"),
	Assert("(λx. y) ((λt. t t) (λx. x x)) ", "y"),
	Assert("(λx. λs. λz. s (x s z)) (λs. (λz. z))", "1"), // (++ 0)
	Assert("(λx. λs. λz. s (x s z)) 1", "2"), // (++ 1)
	Assert("(λx. λs. λz. x (λf.λg.g (f s)) (λg.z) (λm.m))  (λs. (λz. (s z)))", "0"), // (-- 1)
	Assert("(λx. λy. λs. λz. x s (y s z)) (λs. (λz. (s z))) (λs. (λz. (s (s z))))", "3"), // (+ 1 2)
	Assert("(λx. λy. λz. x (y z))  (λs. (λz. z)) (λs. (λz. (s (s z))))", "0"), // (* 0 2)
	Assert("(λx. λy. λz. x (y z)) (λs. (λz. (s z))) (λs. (λz. (s (s z))))", "2"), // (* 1 2)
	Assert("(λx. λy. λz. x (y z)) (λs. (λz. (s (s z)))) (λs. (λz. (s (s z))))", "4"), // (* 2 2)
	Assert("(λx. λy. λz. x (y z)) (λs. (λz. (s (s (s z))))) (λs. (λz. (s (s z))))", "6"), // (* 3 2)
	Assert("-- 2", "1"),
	Assert("++ 2", "3"),
	Assert("+ 3 4", "7"),
	Assert("- 14 6", "8"),
	Assert("* 5 6", "30"),
	Assert("Y (λf. λn. ZERO n 1 (* n (f (-- n)))) 1", "1"), // 1!
	Assert("Y (λf. λn. ZERO n 1 (* n (f (-- n)))) 2", "2"), // 2!
	Assert("Y (λf. λn. ZERO n 1 (* n (f (-- n)))) 3", "6"), // 3!
	Assert("Y (λf. λn. ZERO n 1 (* n (f (-- n)))) 4", "24"), // 4!
	Assert("Y (λf. λn. ZERO n 1 (* (f (-- n)) 2)) 3", "8"), // 2^3
	Assert("Y (λf. λn. ZERO n 1 (* (f (-- n)) 2)) 4", "16"), // 2^3
	Assert("Y (λf. λn. ZERO n 1 (* (f (-- n)) 3)) 2", "9"), // 3^2
	Assert("Y (λf. λn. ZERO n 1 (* (f (-- n)) 3)) 3", "27"), // 3^3
	Assert("Y (λf. λn. ZERO n 1 (ZERO (-- n) 1 (+ (f (- n 1)) (f (- n 2))))) 0", "1"), // fib(0)
	Assert("Y (λf. λn. ZERO n 1 (ZERO (-- n) 1 (+ (f (- n 1)) (f (- n 2))))) 1", "1"), // fib(1)
	Assert("Y (λf. λn. ZERO n 1 (ZERO (-- n) 1 (+ (f (- n 1)) (f (- n 2))))) 2", "2"), // fib(2)
	Assert("Y (λf. λn. ZERO n 1 (ZERO (-- n) 1 (+ (f (- n 1)) (f (- n 2))))) 3", "3"), // fib(3)
	Assert("Y (λf. λn. ZERO n 1 (ZERO (-- n) 1 (+ (f (- n 1)) (f (- n 2))))) 4", "5") // fib(4)
)


fun main(args: Array<String>)
{
	val translator = TranslatorFactory().create()
	val evaluator = Evaluator()

	for (case in TESTS) {
		val actual = evaluator.evaluate(translator.translate(case.source)).evaluateNumbers().toString()
		assert(actual == case.expected, { "`${case.source}` was interpreted to \n `$actual` \n `${case.expected}` expected" })
		print(".")
	}

	println("\n\u001B[32mOK (${TESTS.size} tests)\u001B[0m")
}
